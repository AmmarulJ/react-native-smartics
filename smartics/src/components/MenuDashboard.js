import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import color from '../utils/color';

const MENU_PEMOHON = [
  {
    title: 'Ajukan',
    img: require('../assets/icon-beranda/ajukan.png'),
    target: 'AjukanPerizinan',
  },
  {
    title: 'Panduan',
    img: require('../assets/icon-beranda/panduan.png'),
    target: 'Panduan',
  },
  {
    title: 'Arsip',
    img: require('../assets/icon-beranda/arsip.png'),
    target: 'Arsip',
  },
  {
    title: 'Ulasan',
    img: require('../assets/icon-beranda/ulasan.png'),
    target: 'Ulasan',
  },
  {
    title: 'Survey',
    img: require('../assets/icon-beranda/survey.png'),
    target: 'Survey',
  },

  {
    title: 'Perizinan',
    img: require('../assets/icon-beranda/panduan.png'),
    target: 'MenuPerizinan',
  },
];

export default function MenuDashboard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.containerMenu}>
        {MENU_PEMOHON.map((item, index) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                props.onClick(item.target);
              }}
              style={styles.containerItemMenu}
              key={index}>
              <Image source={item.img} style={styles.img} resizeMode="cover" />
              <Text style={styles.itemTitle}>{item.title}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  containerMenu: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // gap: 38,
  },
  containerItemMenu: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: 58,
    height: 58,
  },
  itemTitle: {
    alignSelf: 'center',
    marginTop: 4,
    fontSize: 16,
    fontWeight: '500',
    color: color.black,
  },
});
