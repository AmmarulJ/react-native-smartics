import React from 'react';
import {View, Image} from 'react-native';
import color from '../utils/color';

export default function CardList(props) {
  return (
    <View
      style={[
        {
          width: 394,
          height: 195,
          backgroundColor: color.white,
          marginBottom: 20,
          borderRadius: 20,
        },
        props.wrapperStyle,
      ]}>
      <Image
        source={require('../assets/gradient-color.png')}
        style={{width: '100%', height: '100%', borderRadius: 20}}
        resizeMode="cover"
      />
      {props.children}
    </View>
  );
}
