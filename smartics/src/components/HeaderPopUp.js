import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import color from '../utils/color';

export default function HeaderPopUp(props) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImg}>
        <Image
          source={require('../assets/logo-header-beranda.png')}
          style={styles.img}
          resizeMode="cover"
        />
      </View>
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Selamat Datang di Smartics
        </Text>
        <Text style={styles.txtContent}>Terdapat 2 Perizinan Yang Masuk</Text>
        <View style={styles.containerButton}>
          <Text style={styles.txtButton}>Lihat Perizinan</Text>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: color.white,
    borderRadius: 20,
    paddingHorizontal: 18,
    paddingVertical: 28,
  },
  containerImg: {
    width: 84,
    height: 84,
  },
  containerTitle: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 18,
  },
  containerButton: {
    flex: 1,
    paddingTop: 12,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  txtTitle: {
    fontSize: 14,
    color: color.black,
    fontWeight: '500',
    marginBottom: 6,
  },
  txtContent: {
    fontSize: 14,
    color: color.black,
    fontWeight: '400',
  },
  txtButton: {
    color: color.primary,
    fontWeight: '600',
    fontSize: 14,
  },
});
