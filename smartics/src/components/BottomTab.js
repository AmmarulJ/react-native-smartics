import React from 'react';
import {Dimensions, Image} from 'react-native';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import color from '../utils/color';

const SCREEN_WIDTH = Dimensions.get('window').width;

const MENUS = [
  {
    label: 'Beranda',
    img: require('../assets/bottomIcons/beranda.png'),
    target: 'Beranda',
  },
  {
    label: 'Perizinan',
    img: require('../assets/bottomIcons/perizinan.png'),
    target: 'Perizinan',
  },
  {
    label: 'Pelacakan',
    img: require('../assets/bottomIcons/pelacakan.png'),
    target: 'Pelacakan',
  },
  {
    label: 'Chat',
    img: require('../assets/bottomIcons/message.png'),
    target: 'Chat',
  },
  {
    label: 'Profil',
    img: require('../assets/bottomIcons/user.png'),
    target: 'Profile',
  },
];

export default function BottomTab(props) {
  return (
    <View style={styles.wrapper}>
      {MENUS.map((menu, key) => {
        const isActive = props.selected === key;
        let buttonStyle = [styles.buttonInternal];
        let contentColor = color.primary;
        let fontWidth = '500';

        if (isActive) {
          // @ts-ignore
          buttonStyle = [styles.buttonInternal];
          contentColor = color.black;
          fontWidth = '800';
        }

        return (
          <TouchableOpacity
            style={styles.button}
            key={key}
            onPress={() => {
              props.onClick(menu.target);
            }}>
            <View style={buttonStyle}>
              <Image
                source={menu.img}
                style={styles.menuImg}
                resizeMode="cover"
              />
              <Text
                style={[
                  styles.textActive,
                  {color: contentColor, fontWeight: fontWidth},
                ]}>
                {menu.label}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: color.white,
    elevation: 5,
  },
  button: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 10,
    flexDirection: 'column',
  },
  buttonInternal: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 50,
  },
  buttonBig: {
    position: 'absolute',
    left: SCREEN_WIDTH / 2 - 50,
    bottom: 0,
    width: 100,
    borderRadius: 57,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    tintColor: color.grayColor,
  },
  iconActive: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
    tintColor: color.primary,
    marginBottom: 5,
  },
  text: {
    fontSize: 11,
    textAlign: 'center',
  },
  textActive: {
    fontSize: 10,
    textAlign: 'center',
    color: color.primary,
    marginTop: 4,
  },
  bigIcon: {
    width: 100,
    height: 100,
  },
  menuImg: {
    width: 24,
    height: 24,
  },
});
