import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import color from '../utils/color';

export default function HeaderBack(props) {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../assets/dashboard.png')}
        style={styles.img}
        resizeMode="cover"
      />
      <View style={styles.containerHeader}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => props.onPress()}
          style={styles.containerIconLeft}>
          <Image
            source={require('../assets/Left.png')}
            style={styles.iconLeft}
            resizeMode="cover"
          />
        </TouchableOpacity>
        <Text style={styles.txtTitle}>{props.title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 71,
    backgroundColor: color.primary,
    justifyContent: 'center',
  },
  img: {
    position: 'absolute',
    width: 430,
    height: 71,
  },
  txtTitle: {
    fontSize: 20,
    fontWeight: '600',
    color: color.black,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconLeft: {
    width: 20,
    height: 20,
  },
  containerIconLeft: {
    paddingHorizontal: 27,
    marginVertical: 26,
  },
});
