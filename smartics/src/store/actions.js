import * as types from "./constants"

export const setUser = (payload) => (dispatch) => {
    dispatch({
        type: types.SET_USER,
        payload,
    });
};

export const setSplashScreen = (payload) => (dispatch) => {
    dispatch({
        type: types.SET_SPLASH_SCREEN,
        payload,
    });
};