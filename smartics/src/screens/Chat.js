import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';

export default function Chat(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderTitle title="Chat" />
        <ScrollView>
          <Text>ini adalah Chat</Text>
        </ScrollView>

        <BottomTab
          selected={3}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
