import React, {useCallback} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderBeranda from '../components/HeaderBeranda';
import color from '../utils/color';
import MenuDashboard from '../components/MenuDashboard';
import NoData from '../components/NoData';
import HeaderPopUp from '../components/HeaderPopUp';
import CardList from '../components/CardList';
import Button from '../components/Button';

export default function Beranda(props) {
  const navigation = useNavigation();

  const renderAdminUtama = useCallback(() => {
    return (
      <CardList>
        <View style={{position: 'absolute'}}>
          <View
            style={{
              paddingTop: 26,
              paddingLeft: 30,
              marginBottom: 13,
            }}>
            <Text style={{fontSize: 15, fontWeight: '600', color: color.black}}>
              Jenis Perizinan
            </Text>
            <Text>N/A</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: 30,
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Nomor Surat
              </Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'column', width: '50%'}}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Status
              </Text>
              <Text>N/A</Text>
            </View>
          </View>

          <View
            style={{
              marginTop: 28,
              paddingLeft: 30,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Verifikasi Verifikator
              </Text>
              <Text>N/A</Text>
            </View>
            <Button
              theme="primary"
              style={{width: 143, height: 35}}
              textStyle={{fontSize: 14, marginLeft: 9}}
              onPress={() => {
                navigation.navigate('DetailPerizinan');
              }}>
              Lihat Detail
            </Button>
          </View>
        </View>
      </CardList>
    );
  }, [navigation]);

  const renderAudit = useCallback(() => {
    return (
      <CardList wrapperStyle={{height: 80}}>
        <View style={{position: 'absolute'}}>
          <View
            style={{
              paddingTop: 20,
              flexDirection: 'row',
              marginLeft: 30,
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Periode
              </Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'column', width: '50%'}}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Hasil Laporan
              </Text>
              <Text>N/A</Text>
            </View>
          </View>
        </View>
      </CardList>
    );
  }, []);

  const renderAuditArsip = useCallback(() => {
    return (
      <CardList>
        <View style={{position: 'absolute'}}>
          <View
            style={{
              paddingTop: 26,
              paddingLeft: 30,
              marginBottom: 13,
            }}>
            <Text style={{fontSize: 15, fontWeight: '600', color: color.black}}>
              Jenis Perizinan
            </Text>
            <Text>N/A</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: 30,
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Nomor Surat
              </Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'column', width: '50%'}}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Status
              </Text>
              <Text>N/A</Text>
            </View>
          </View>

          <View
            style={{
              marginTop: 28,
              paddingLeft: 30,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Tanggal
              </Text>
              <Text>N/A</Text>
            </View>
            <Button
              theme="primary"
              style={{width: 143, height: 35}}
              textStyle={{fontSize: 14, marginLeft: 9}}
              onPress={() => {
                navigation.navigate('DetailPerizinan');
              }}>
              Lihat Detail
            </Button>
          </View>
        </View>
      </CardList>
    );
  }, [navigation]);

  const renderWalikota = useCallback(() => {
    return (
      <CardList>
        <View style={{position: 'absolute'}}>
          <View
            style={{
              paddingTop: 26,
              paddingLeft: 30,
              marginBottom: 13,
            }}>
            <Text style={{fontSize: 15, fontWeight: '600', color: color.black}}>
              Jenis Perizinan
            </Text>
            <Text>N/A</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              marginLeft: 30,
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Nomor Surat
              </Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'column', width: '50%'}}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Status
              </Text>
              <Text>N/A</Text>
            </View>
          </View>

          <View
            style={{
              marginTop: 28,
              paddingLeft: 30,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '48%',
              }}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Verifikasi Verifikator
              </Text>
              <Text>N/A</Text>
            </View>
            <Button
              theme="primary"
              style={{width: 143, height: 35}}
              textStyle={{fontSize: 14, marginLeft: 9}}
              onPress={() => {
                navigation.navigate('UlasanDetail', {
                  data: {
                    label: 'Daftar Ulang Izin Operasional',
                    nomor: '22330',
                    tanggal: '2001-10-10',
                  },
                });
              }}>
              Hasil Survey
            </Button>
          </View>
        </View>
      </CardList>
    );
  }, [navigation]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor={color.primary} barStyle="dark-content" />
        <HeaderBeranda />
        <ScrollView style={styles.mainContent}>
          <View style={styles.containerMain}>
            <HeaderPopUp />
          </View>
          <View style={styles.content}>
            <MenuDashboard
              onClick={event => {
                navigation.navigate(event);
              }}
            />
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Permohonan Terbaru</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('Perizinan');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>
                <NoData label="Anda Belum Memiliki Permohonan" />
              </View>
            </View>
            {/* punya role admin */}
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Perizinan Terbaru</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('ListPerizinanTerbaru');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>
                {/* <NoData label="Anda Belum Memiliki Permohonan" /> */}
                {renderAdminUtama()}
              </View>
            </View>
            {/*  */}

            {/* mode auditor */}
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Laporan Audit Terbaru</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('LaporanAudit');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>{renderAudit()}</View>
            </View>
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Daftar Arsip</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('Arsip');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>{renderAuditArsip()}</View>
            </View>
            {/*  */}

            {/* role bupati/walikote */}
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Ulasan Terbaru</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('ListDaftarUlasan');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>{renderWalikota()}</View>
            </View>
            {/*  */}
            <View style={styles.childContent}>
              <View style={styles.rowContent}>
                <Text style={styles.txtLabeltitle}>Notifikasi Terbaru</Text>
                <View style={styles.gapFlex} />
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate('Notifikasi');
                  }}>
                  <Text style={styles.txtColorPrimary}>Selengkapnya</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemContent}>
                <NoData label="Anda Belum Notifikasi Permohonan" />
              </View>
            </View>
          </View>
        </ScrollView>

        <BottomTab
          selected={0}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginHorizontal: 23,
  },
  itemContent: {
    marginTop: 18,
  },
  txtLabeltitle: {
    color: color.black,
    fontSize: 18,
    fontWeight: '600',
  },
  txtColorPrimary: {
    color: color.primary,
    fontSize: 14,
    fontWeight: '600',
  },
  mainContent: {
    flex: 1,
    marginTop: -110,
  },
  childContent: {
    flexDirection: 'column',
    paddingVertical: 25,
  },
  gapFlex: {
    flex: 1,
  },
  rowContent: {
    flexDirection: 'row',
  },
  containerMain: {
    paddingHorizontal: 23,
    paddingBottom: 23,
    paddingTop: 13,
  },
});
