import React from 'react';
import {SafeAreaView, Text, ScrollView, StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import CardList from '../components/CardList';
import color from '../utils/color';

export default function LaporanAudit(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Laporan Audit" onPress={() => navigation.goBack()} />
        <ScrollView style={styles.mainContent}>
          <CardList wrapperStyle={{height: 80}}>
            <View style={{position: 'absolute'}}>
              <View
                style={{
                  paddingTop: 20,
                  flexDirection: 'row',
                  marginLeft: 30,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: '600',
                      color: color.black,
                    }}>
                    Periode
                  </Text>
                  <Text>N/A</Text>
                </View>
                <View style={{flexDirection: 'column', width: '50%'}}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: '600',
                      color: color.black,
                    }}>
                    Hasil Laporan
                  </Text>
                  <Text>N/A</Text>
                </View>
              </View>
            </View>
          </CardList>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  mainContent: {
    flex: 1,
    marginHorizontal: 23,
    marginVertical: 20,
  },
});
