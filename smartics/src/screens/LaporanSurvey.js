import React from 'react';
import {SafeAreaView, Text, ScrollView, StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import Button from '../components/Button';
import TextInputIcon from '../components/TextInputIcon';
import CardList from '../components/CardList';
import color from '../utils/color';
import {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function LaporanSurvey(props) {
  const navigation = useNavigation();
  const [search, setSearch] = useState('');
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Laporan Survey"
          onPress={() => navigation.goBack()}
        />
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 23,
            paddingTop: 15,
            justifyContent: 'space-between',
          }}>
          <TextInputIcon
            isIcon={true}
            jenisIcons="Ionicons"
            iconName="search-outline"
            placeholder="Search"
            value={search}
            onChangeText={setSearch}
            containerStyle={styles.input}
          />
          <View
            style={{
              backgroundColor: color.primary,
              padding: 10,
              borderRadius: 10,
            }}>
            <Ionicons name="options-outline" size={24} color={color.white} />
          </View>
        </View>
        <ScrollView style={{flex: 1, paddingHorizontal: 23}}>
          <View style={{paddingVertical: 20}}>
            <Text style={{fontSize: 20, fontStyle: '700', color: color.black}}>
              Permohonan Perizinan Selesai
            </Text>
          </View>
          <CardList>
            <View style={{position: 'absolute'}}>
              <View style={{paddingTop: 26, paddingLeft: 30, marginBottom: 13}}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>
                  Jenis Perizinan
                </Text>
                <Text>N/A</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 30,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>
                    Nomor Surat
                  </Text>
                  <Text>N/A</Text>
                </View>
                <View style={{flexDirection: 'column', width: '50%'}}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>Statuss</Text>
                  <Text>N/A</Text>
                </View>
              </View>

              <View
                style={{marginTop: 28, paddingLeft: 30, flexDirection: 'row'}}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>
                    Verifikasi Verifikator
                  </Text>
                  <Text>N/A</Text>
                </View>
                <Button
                  theme="primary"
                  style={{width: 143, height: 35}}
                  textStyle={{fontSize: 14, marginLeft: 9}}
                  onPress={() => {
                    navigation.navigate('DetailLaporanSurvey');
                  }}>
                  Hasil Survey
                </Button>
              </View>
            </View>
          </CardList>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    flex: 1,
    marginRight: 10,
  },
  label: {
    fontSize: 13,
    marginBottom: 5,
  },
});
