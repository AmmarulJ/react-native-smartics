import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import NoData from '../components/NoData';

export default function Notifikasi(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <HeaderBack
        title="Semua Notifikasi"
        onPress={() => navigation.goBack()}
      />
      <ScrollView style={styles.mainContent}>
        <View style={styles.itemContent}>
          <NoData label="Anda Belum Memiliki Notifikasi" />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContent: {
    flex: 1,
    paddingHorizontal: 23,
  },

  itemContent: {
    marginTop: 18,
  },
});
