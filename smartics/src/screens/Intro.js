import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';

export default function Intro(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={require('../assets/intro-logo.png')}
          style={styles.img}
          resizeMode="cover"
        />
      </View>
      <View style={styles.bottomWrapper}>
        <Text style={styles.labelTitle}>
          Dengan Smartics Membuat Anda Lebih Bersantai
        </Text>
        <Text style={styles.txtContent}>
          Anda Dapat Mengajukan perizinan dibidang pendidikan dengan cepat,
          buruan mulai sekarang!
        </Text>
      </View>
      <View style={styles.buttonWrapper}>
        <Button onPress={() => navigation.navigate('Login')}>
          Mulai Sekarang
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  img: {
    height: '100%',
    width: '100%',
  },
  bottomWrapper: {
    backgroundColor: color.white,
    borderTopLeftRadius: 75,
    borderTopRightRadius: 75,
    justifyContent: 'center',
    flex: 1,
    marginTop: 80,
    paddingHorizontal: 20,
  },
  labelTitle: {
    fontSize: 28,
    color: color.black,
    fontWeight: '600',
    textAlign: 'center',
    marginTop: -58,
  },
  txtContent: {
    fontSize: 18,
    marginTop: 20,
    color: color.black,
    fontWeight: '400',
    textAlign: 'center',
  },
  containerImage: {
    width: '100%',
    height: 369,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonWrapper: {
    position: 'absolute',
    bottom: 40,
    width: '80%',
    alignSelf: 'center',
  },
});
