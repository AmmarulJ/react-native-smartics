import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';

export default function Pelacakan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderTitle title="Pelacakan" />
        <ScrollView>
          <Text>ini adalah Pelacakan</Text>
        </ScrollView>

        <BottomTab
          selected={2}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
