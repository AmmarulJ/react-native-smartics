import React, {useState} from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import Button from '../components/Button';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';

export default function UlasanPelayanan(props) {
  const navigation = useNavigation();
  const [ulasan, setUlasan] = useState('');
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Ulasan Pelayanan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={{flex: 1, paddingHorizontal: 23}}>
          <View
            style={{
              backgroundColor: color.white,
              paddingVertical: 23,
              paddingHorizontal: 23,
              borderRadius: 10,
              marginVertical: 20,
            }}>
            <View style={{marginBottom: 10}}>
              <Text
                style={{fontSize: 15, fontWeight: '600', color: color.black}}>
                Jenis Perizinan
              </Text>
              <Text>N/A</Text>
            </View>
            <Text style={{fontSize: 15, fontWeight: '600', color: color.black}}>
              Nomor Surat
            </Text>
            <Text>N/A</Text>
          </View>
          <Text
            style={{
              fontSize: 18,
              fontWeight: '500',
              marginBottom: 6,
              color: color.black,
            }}>
            Ulasan
          </Text>
          <View>
            <TextInputIcon
              isIcon={true}
              placeholder="Anda Dapat Menceritakan Pengalaman
          Menggunakan Smartics dan Kritik Saran"
              wrapperStyle={styles.wrapperText}
              multiline={true}
              value={ulasan}
              onChangeText={setUlasan}
              containerStyle={styles.input}
            />
          </View>
        </ScrollView>
        <View style={{backgroundColor: color.white, padding: 26}}>
          <Button>Kirim Ulasan</Button>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    marginBottom: 24,
  },
  wrapperText: {
    height: 258,
    textAlign: 'left',
    alignItems: 'flex-start',
  },
});
