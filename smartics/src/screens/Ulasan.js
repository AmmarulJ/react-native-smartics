import React, {useCallback} from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';
import CardList from '../components/CardList';

const dataDummy = [
  {
    label: 'Daftar Ulang Izin Operasional',
    nomor: '22330',
    tanggal: '2001-10-10',
  },
  {
    label: 'Daftar Ulang Izin Operasional',
    nomor: '22330',
    tanggal: '2001-10-10',
  },
];

export default function Ulasan(props) {
  const navigation = useNavigation();

  // const renderCard = useCallback(data => {
  //   data.map((item, index) => {
  //     return (

  //     );
  //   });
  // }, []);

  return (
    <View style={{flex: 1}}>
      <HeaderBack title="Daftar Ulasan" onPress={() => navigation.goBack()} />
      <ScrollView style={{flex: 1, paddingHorizontal: 23}}>
        <View style={{flexDirection: 'column', marginTop: 20}}>
          {dataDummy.map((item, index) => {
            return (
              <CardList key={index}>
                <View style={{position: 'absolute'}}>
                  <View
                    style={{paddingTop: 26, paddingLeft: 30, marginBottom: 13}}>
                    <Text style={{fontSize: 15, fontWeight: '600'}}>
                      Jenis Perizinan
                    </Text>
                    <Text>{item.label}</Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      marginLeft: 30,
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        width: '48%',
                      }}>
                      <Text style={{fontSize: 15, fontWeight: '600'}}>
                        Nomor Surat
                      </Text>
                      <Text>{item.nomor}</Text>
                    </View>
                    <View style={{flexDirection: 'column', width: '50%'}}>
                      <Text style={{fontSize: 15, fontWeight: '600'}}>
                        Tanggal
                      </Text>
                      <Text>{item.tanggal}</Text>
                    </View>
                  </View>

                  <View style={{marginTop: 28, paddingLeft: 30}}>
                    <Button
                      theme="primary"
                      style={{width: 143, height: 35}}
                      textStyle={{fontSize: 14, marginLeft: 9}}
                      onPress={() => {
                        navigation.navigate('UlasanDetail', {data: item});
                      }}>
                      Lihat Detail
                    </Button>
                  </View>
                </View>
              </CardList>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}
