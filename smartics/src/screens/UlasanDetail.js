import React from 'react';
import {
  SafeAreaView,
  Text,
  ScrollView,
  StyleSheet,
  View,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';

export default function UlasanDetail(props) {
  const navigation = useNavigation();
  const {data} = props.route.params;
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Detail Ulasan" onPress={() => navigation.goBack()} />
        <ScrollView style={{flex: 1, paddingHorizontal: 23, paddingTop: 20}}>
          <View
            style={{
              width: 394,
              height: 230,
              backgroundColor: color.white,
              marginBottom: 20,
              borderRadius: 20,
            }}>
            <Image
              source={require('../assets/gradient-color.png')}
              style={{width: '100%', height: '100%', borderRadius: 20}}
              resizeMode="cover"
            />

            <View style={{position: 'absolute'}}>
              <View style={{paddingTop: 26, paddingLeft: 30, marginBottom: 13}}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>
                  Jenis Perizinan
                </Text>
                <Text>{data.label}</Text>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 30,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>
                    Nomor Surat
                  </Text>
                  <Text>{data.nomor}</Text>
                </View>
                <View style={{flexDirection: 'column', width: '50%'}}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>Tanggal</Text>
                  <Text>{data.tanggal}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 30,
                  marginTop: 13,
                }}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>
                  Isi Ulasan
                </Text>
                <View style={{width: '80%'}}>
                  <Text>
                    Lorem ipsun dolor sitamet Lorem ipsun dolor sitamet Lorem
                    ipsun dolor sitamet Lorem ipsun dolor sitamet Lorem ipsun
                    dolor sitamet Lorem ipsun dolor sitamet Lorem ipsun dolor
                    sitamet Lorem ipsun dolor sitamet
                  </Text>
                </View>
                <View style={{height: 20}} />
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
