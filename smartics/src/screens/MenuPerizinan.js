import React from 'react';
import {
  SafeAreaView,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';

const menu = [
  {
    label: 'Semua Perizinan',
    image: require('../assets/semua-perizinan.png'),
    target: 'SemuaPerizinan',
  },
  {
    label: 'Perizinan Masuk',
    image: require('../assets/perizinan-masuk.png'),
    target: 'PerizinanMasuk',
  },
  {
    label: 'Perizinan Terlambat',
    image: require('../assets/jadwal-survey.png'),
    target: 'PerizinanTerlambat',
  },
];

export default function MenuPerizinan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Perizinan" onPress={() => navigation.goBack()} />
        <ScrollView
          style={{flex: 1, paddingHorizontal: 23, paddingVertical: 20}}>
          {menu.map((item, index) => {
            return (
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.containerCard}
                key={index}
                onPress={() => {
                  navigation.navigate(item.target);
                }}>
                <View style={{height: 30, width: 24, marginHorizontal: 10}}>
                  <Image
                    source={item.image}
                    style={styles.img}
                    resizeMode="cover"
                  />
                </View>
                <Text>{item.label}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    height: '100%',
    width: '100%',
  },
  containerCard: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: color.white,
    marginBottom: 15,
    borderRadius: 10,
  },
});
