import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';
import HeaderBack from '../components/HeaderBack';

export default function DetailPerizinan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Detail Perizinan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={{paddingHorizontal: 23, paddingVertical: 23}}>
          <View
            style={{
              backgroundColor: color.white,
              paddingHorizontal: 18,
              paddingVertical: 18,
              borderRadius: 20,
            }}>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Jenis Perizinan</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Nomor Surat</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Status</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Tanggal</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jadwal Survey</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Alamat</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Surat Permohonan</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Dokumen Hak Milik</Text>
              <Text>N/A</Text>
            </View>
          </View>
        </ScrollView>
        <View style={{backgroundColor: color.white, padding: 26}}>
          <Button
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('ChatDetail');
            }}>
            Chat Admin
          </Button>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Label: {
    fontSize: 15,
    fontWeight: '600',
    color: color.black,
  },
});
