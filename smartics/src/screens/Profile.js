import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import color from '../utils/color';
import Button from '../components/Button';
import {useDispatch} from 'react-redux';
import {setUser} from '../store/actions';

let data = [
  {
    label: 'Nama',
  },
  {
    label: 'Email',
  },
  {
    label: 'Nomor Telepon',
  },
  {
    label: 'Tanggal Lahir',
  },
  {
    label: 'Jenis Kelamin',
  },
  {
    label: 'Jenis Identitas',
  },
  {
    label: 'Nomor Identitas',
  },
  {
    label: 'Provinsi',
  },
  {
    label: 'Kota',
  },
  {
    label: 'Kecamatan',
  },
  {
    label: 'Kelurahan',
  },
  {
    label: 'Pekerjaan',
  },
];

export default function Profile(props) {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderTitle title="Profile" />
        <ScrollView
          style={styles.mainContainer}
          showsVerticalScrollIndicator={false}>
          <View style={styles.containerProfile}>
            <View style={styles.containerImage}>
              <Image
                source={require('../assets/profile.png')}
                style={styles.img}
                resizeMode="cover"
              />
            </View>
            {data.map((item, index) => {
              return (
                <View key={index} style={styles.containerTxt}>
                  <Text style={styles.txtLabel}>{item.label}</Text>
                </View>
              );
            })}
            <View>
              <Text>
                Nb: Untuk Informasi lebih lengkap atau memperbaiki informasi
                silahkan akses website smartics
              </Text>
            </View>
            <View style={styles.gapBottom} />
          </View>
          <View style={{alignSelf: 'center', marginBottom: 38}}>
            <Button
              theme="logout"
              jenisIcons="Ionicons"
              iconName="exit-outline"
              colorIcon={color.white}
              iconSize={15}
              textStyle={{fontSize: 14, marginLeft: 9}}
              loading={false}
              onPress={() => {
                dispatch(setUser(null));
              }}>
              Keluar
            </Button>
          </View>
        </ScrollView>

        <BottomTab
          selected={4}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerProfile: {
    paddingHorizontal: 29,
    borderRadius: 10,
    marginVertical: 27,
    backgroundColor: color.white,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  containerImage: {
    width: 115,
    height: 115,
    alignSelf: 'center',
    marginVertical: 47,
  },
  mainContainer: {
    flex: 1,
    marginHorizontal: 25,
  },
  gapBottom: {
    height: 29,
  },
  containerTxt: {
    marginBottom: 15,
  },
  txtLabel: {
    fontSize: 16,
    fontWeight: '600',
  },
});
