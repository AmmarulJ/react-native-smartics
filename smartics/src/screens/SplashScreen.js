import React, {useEffect} from 'react';
import {StyleSheet, Text, StatusBar, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import {setSplashScreen} from '../store/actions';
import color from '../utils/color';
import {View} from 'react-native';
export default function SplashScreen(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(setSplashScreen(false));
    }, 2000);
  }, [dispatch]);

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={color.primary} barStyle="light-content" />
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{height: 200, width: 200, alignSelf: 'center'}}>
          <Image
            source={require('../assets/logo-smartics.png')}
            style={styles.img}
            resizeMode="cover"
          />
        </View>
        <View style={{alignSelf: 'center', marginBottom: 31, marginTop: 18}}>
          <Text style={{fontSize: 34, fontWeight: '600', color: color.black}}>
            Smartics
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.primary,
  },
  top: {
    flex: 1,
    justifyContent: 'center',
  },
  bottom: {
    height: 200,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    width: 400,
    height: 200,
  },
  text: {
    width: 300,
    height: 65,
  },
  center: {
    fontSize: 18,
    color: color.black,
    textAlign: 'center',
  },
  img: {
    width: '100%',
    height: '100%',
  },
});
