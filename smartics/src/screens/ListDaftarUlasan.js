import React, {useCallback} from 'react';
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';
import CardList from '../components/CardList';
import TextInputIcon from '../components/TextInputIcon';
import {useState} from 'react';

const dataDummy = [
  {
    label: 'Daftar Ulang Izin Operasional',
    nomor: '22330',
    tanggal: '2001-10-10',
  },
  {
    label: 'Daftar Ulang Izin Operasional',
    nomor: '22330',
    tanggal: '2001-10-10',
  },
];

export default function ListDaftarUlasan(props) {
  const navigation = useNavigation();
  const [search, setSearch] = useState('');
  // const renderCard = useCallback(data => {
  //   data.map((item, index) => {
  //     return (

  //     );
  //   });
  // }, []);

  return (
    <View style={{flex: 1}}>
      <HeaderBack title="Daftar Ulasan" onPress={() => navigation.goBack()} />
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 23,
          paddingTop: 15,
          justifyContent: 'space-between',
        }}>
        <TextInputIcon
          isIcon={true}
          jenisIcons="Ionicons"
          iconName="search-outline"
          placeholder="Search"
          value={search}
          onChangeText={setSearch}
          containerStyle={styles.input}
        />
      </View>
      <ScrollView style={{flex: 1, paddingHorizontal: 23}}>
        <View style={{flexDirection: 'column', marginTop: 20}}>
          {dataDummy.map((item, index) => {
            return (
              <CardList key={index}>
                <View style={{position: 'absolute'}}>
                  <View
                    style={{paddingTop: 26, paddingLeft: 30, marginBottom: 13}}>
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: '600',
                        color: color.black,
                      }}>
                      Jenis Perizinan
                    </Text>
                    <Text>{item.label}</Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      marginLeft: 30,
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        width: '48%',
                      }}>
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: '600',
                          color: color.black,
                        }}>
                        Nomor Surat
                      </Text>
                      <Text>{item.nomor}</Text>
                    </View>
                    <View style={{flexDirection: 'column', width: '50%'}}>
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: '600',
                          color: color.black,
                        }}>
                        Tanggal
                      </Text>
                      <Text>{item.tanggal}</Text>
                    </View>
                  </View>

                  <View
                    style={{
                      marginTop: 28,
                      paddingLeft: 30,
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        width: '48%',
                      }}>
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: '600',
                          color: color.black,
                        }}>
                        Verifikasi Verifikator
                      </Text>
                      <Text>N/A</Text>
                    </View>
                    <Button
                      theme="primary"
                      style={{width: 143, height: 35}}
                      textStyle={{fontSize: 14, marginLeft: 9}}
                      onPress={() => {
                        navigation.navigate('UlasanDetail', {data: item});
                      }}>
                      Lihat Detail
                    </Button>
                  </View>
                </View>
              </CardList>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    flex: 1,
    marginRight: 10,
  },
  label: {
    fontSize: 13,
    marginBottom: 5,
  },
});
