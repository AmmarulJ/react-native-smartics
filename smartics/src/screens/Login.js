import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import Button from '../components/Button';
import color from '../utils/color';
import app from '../config/app';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import {setUser} from '../store/actions';
import {ScrollView} from 'react-native-gesture-handler';

export default function Login({navigation}) {
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [email, setEmail] = useState(__DEV__ ? app.EXAMPLE_EMAIL : '');
  const [password, setPassword] = useState(__DEV__ ? app.EXAMPLE_PASSWORD : '');

  // const login = useCallback(() => {
  //     setLoading(true);
  //     let data = { username, password };
  //     HttpRequest.login(data).then((res) => {
  //         console.log("Res", res.data);
  //         Toast.showSuccess("Login Success");
  //         setLoading(false);
  //         dispatch(setUser(res.data));
  //     }).catch((err) => {
  //         console.log(err, err.response);
  //         Toast.showError(err.response.data.message);
  //         setLoading(false);
  //     });
  // }, [username, password]);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.content}>
        <View style={{height: 95, width: 91, alignSelf: 'center'}}>
          <Image
            source={require('../assets/logo-smartics.png')}
            style={styles.img}
            resizeMode="cover"
          />
        </View>
        <View style={{alignSelf: 'center', marginBottom: 31, marginTop: 18}}>
          <Text style={{fontSize: 24, fontWeight: '600', color: color.black}}>
            Masuk Smartics
          </Text>
        </View>
        {/* <Text style={styles.titleName}>{app.NAME}</Text>
        <Text style={[styles.titleName, {fontSize: 18, alignSelf: 'center'}]}>
          Masuk
        </Text> */}

        <Text style={styles.label}>Email</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
          containerStyle={styles.input}
        />

        <Text style={styles.label}>Password</Text>

        <TextInputIcon
          isIcon={true}
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
          containerStyle={styles.input}
        />

        <View style={styles.containerText}>
          <Text style={styles.txtLabel}>Belum punya akun? </Text>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('Register');
            }}>
            <Text style={styles.txtLabelPrimary}>Daftar Sekarang</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      <View style={styles.wrapperButton}>
        <Button loading={isLoading} onPress={() => dispatch(setUser([]))}>
          Masuk
        </Button>
        <View style={{marginVertical: 6}} />
        <Button
          theme={'white'}
          jenisIcons="Ionicons"
          iconName="logo-google"
          colorIcon={color.black}
          iconSize={15}
          loading={isLoading}
          textStyle={{fontSize: 20, marginLeft: 9}}
          onPress={() => dispatch(setUser([]))}>
          Masuk Dengan Google
        </Button>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 20,
  },
  titleName: {
    fontSize: 20,
    fontFamily: fonts.montserratBold,
    marginBottom: 20,
  },
  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 13,
    fontFamily: fonts.montserratReguler,
    marginBottom: 5,
  },

  bottomWrap: {
    flex: 4,
    paddingHorizontal: 20,
  },

  lineWrap: {
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#333333',
  },
  textBetween: {
    color: color.white,
    marginHorizontal: 10,
    fontSize: 14,
  },

  buttonNoBg: {
    backgroundColor: color.black,
    // marginBottom: 30,
    marginHorizontal: 20,
    borderWidth: 0,
  },
  signInText: {
    fontWeight: 'bold',
    color: color.primary,
  },

  imageWallpaper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },

  footerForm: {
    flexDirection: 'row',
    marginBottom: 26,
    alignItems: 'center',
  },
  checkBox: {
    width: 24,
    height: 24,
  },
  txtForgotPassword: {
    fontSize: 16,
    textAlign: 'right',
    color: color.primary,
    fontFamily: fonts.montserratBold,
  },
  txtRememberMe: {
    fontSize: 16,
    flex: 1,
    marginLeft: 8,
    color: color.Neutral80,
    fontFamily: fonts.montserratReguler,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  txtLabel: {
    fontSize: 16,
  },
  txtLabelPrimary: {
    color: color.primary,
    fontSize: 16,
    marginLeft: 5,
  },
  containerText: {
    marginTop: 18,
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapperButton: {
    flexDirection: 'column',
    paddingHorizontal: 37,
    marginBottom: 20,
  },
});
