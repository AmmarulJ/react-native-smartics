import React from 'react';
import {SafeAreaView, Text, ScrollView, StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';

export default function DetailLaporanSurvey(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Detail Laporan Survey"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={{paddingHorizontal: 23, paddingVertical: 23}}>
          <View
            style={{
              backgroundColor: color.white,
              paddingHorizontal: 18,
              paddingVertical: 18,
              borderRadius: 20,
            }}>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Jenis Perizinan</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Nama Surveyor</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Nomor Surat</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Alamat</Text>
              <Text>N/A</Text>
            </View>

            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jadwal Survey</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Verifikasi Verifikator</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Alamat Survey</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Longitude</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Latitude</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Berkas Perizinan</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Hasil Laporan</Text>
                <Text>N/A</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Label: {
    fontSize: 15,
    fontWeight: '600',
    color: color.black,
  },
});
