import React, {useState} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import Button from '../components/Button';
import CardList from '../components/CardList';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function PerizinanTerlambat(props) {
  const navigation = useNavigation();
  const [search, setSearch] = useState('');
  return (
    <View style={{flex: 1}}>
      <HeaderBack
        title="Perizinan Terlambat"
        onPress={() => navigation.goBack()}
      />
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 23,
          paddingTop: 15,
          justifyContent: 'space-between',
        }}>
        <TextInputIcon
          isIcon={true}
          jenisIcons="Ionicons"
          iconName="search-outline"
          placeholder="Search"
          value={search}
          onChangeText={setSearch}
          containerStyle={styles.input}
        />
      </View>
      <ScrollView style={{flex: 1, paddingHorizontal: 23, paddingVertical: 20}}>
        <CardList>
          <View style={{position: 'absolute'}}>
            <View style={{paddingTop: 26, paddingLeft: 30, marginBottom: 13}}>
              <Text style={{fontSize: 15, fontWeight: '600'}}>
                Jenis Perizinan
              </Text>
              <Text>N/A</Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 30,
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  width: '48%',
                }}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>
                  Nomor Surat
                </Text>
                <Text>N/A</Text>
              </View>
              <View style={{flexDirection: 'column', width: '50%'}}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>Statuss</Text>
                <Text>N/A</Text>
              </View>
            </View>

            <View
              style={{marginTop: 28, paddingLeft: 30, flexDirection: 'row'}}>
              <View
                style={{
                  flexDirection: 'column',
                  width: '48%',
                }}>
                <Text style={{fontSize: 15, fontWeight: '600'}}>Tanggal</Text>
                <Text>N/A</Text>
              </View>
              <Button
                theme="primary"
                style={{width: 143, height: 35}}
                textStyle={{fontSize: 14, marginLeft: 9}}
                onPress={() => {
                  navigation.navigate('DetailPerizinan');
                }}>
                Lihat Detail
              </Button>
            </View>
          </View>
        </CardList>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    flex: 1,
    marginRight: 10,
  },
  label: {
    fontSize: 13,
    marginBottom: 5,
  },
});
