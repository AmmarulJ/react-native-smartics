import React, {useState} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import Combobox from '../components/Combobox';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import Button from '../components/Button';

let dataJenisPerizinan = [
  {
    id: '1',
    label: 'Daftar Ulang Izin Operasional',
  },
  {
    id: '2',
    label: 'Daftar Ulang Izin OperasionPerizinan Pendirian',
  },
  {
    id: '3',
    label: 'Perizinan Operasional',
  },
  {
    id: '4',
    label: 'Perizinan Perubahan',
  },
  {
    id: '5',
    label: 'Rekomendasi Satuan Pendidikan Kerjasama',
  },
  {
    id: '5',
    label: 'Pencabutan Izin',
  },
];
let dataKategoriPerizinan = [
  {
    id: '1',
    label: 'Paud',
  },
  {
    id: '2',
    label: 'TK',
  },
  {
    id: '3',
    label: 'SD',
  },
  {
    id: '4',
    label: 'SMP',
  },
];

export default function AjukanPerizinan(props) {
  const navigation = useNavigation();

  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');

  return (
    <View style={{flex: 1}}>
      <HeaderBack
        title="Ajukan Perizinan"
        onPress={() => navigation.goBack()}
      />
      <ScrollView style={styles.container}>
        <Combobox
          showSearchBar={false}
          label={'Jenis Perizinan'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataJenisPerizinan}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            // setSelectedPelanggan(val)
            console.log('val', val);
          }}
        />
        <Combobox
          showSearchBar={false}
          label={'Kategori Perizinan'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataKategoriPerizinan}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            // setSelectedPelanggan(val)
            console.log('val', val);
          }}
        />
        <Text style={styles.label}>Nama</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Nama"
          value={nama}
          onChangeText={setNama}
          containerStyle={styles.input}
        />
        <Text style={styles.label}>Alamat</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Alamat"
          value={alamat}
          onChangeText={setAlamat}
          containerStyle={styles.input}
        />
      </ScrollView>
      <View style={{backgroundColor: color.white, padding: 26}}>
        <Button>Lanjut</Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 23,
    paddingVertical: 20,
  },
  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 13,
    // fontFamily: fonts.montserratReguler,
    marginBottom: 5,
  },
});
