import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Combobox from '../components/Combobox';

let dataJenisPerizinan = [
  {
    id: '1',
    label: 'Daftar Ulang Izin Operasional',
  },
  {
    id: '2',
    label: 'Daftar Ulang Izin OperasionPerizinan Pendirian',
  },
  {
    id: '3',
    label: 'Perizinan Operasional',
  },
  {
    id: '4',
    label: 'Perizinan Perubahan',
  },
  {
    id: '5',
    label: 'Rekomendasi Satuan Pendidikan Kerjasama',
  },
  {
    id: '5',
    label: 'Pencabutan Izin',
  },
];

export default function Panduan(props) {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1}}>
      <HeaderBack title="Panduan" onPress={() => navigation.goBack()} />
      <ScrollView style={{flex: 1, marginHorizontal: 28, marginTop: 20}}>
        <Combobox
          showSearchBar={false}
          label={'Jenis Perizinan'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataJenisPerizinan}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            // setSelectedPelanggan(val)
            console.log('val', val);
          }}
        />
      </ScrollView>
    </View>
  );
}
