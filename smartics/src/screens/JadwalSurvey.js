import React from 'react';
import {SafeAreaView, Text, ScrollView, StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import CardList from '../components/CardList';
import Button from '../components/Button';

export default function JadwalSurvey(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Jadwal Survey" onPress={() => navigation.goBack()} />
        <ScrollView
          style={{flex: 1, paddingHorizontal: 23, paddingVertical: 20}}>
          <CardList wrapperStyle={{height: 159}}>
            <View style={{position: 'absolute'}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginLeft: 30,
                  paddingTop: 26,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>
                    Nomor Surat
                  </Text>
                  <Text>N/A</Text>
                </View>
                <View style={{flexDirection: 'column', width: '50%'}}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>Status</Text>
                  <Text>N/A</Text>
                </View>
              </View>

              <View
                style={{marginTop: 28, paddingLeft: 30, flexDirection: 'row'}}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '48%',
                  }}>
                  <Text style={{fontSize: 15, fontWeight: '600'}}>Tanggal</Text>
                  <Text>N/A</Text>
                </View>
                <Button
                  theme="primary"
                  style={{width: 143, height: 35}}
                  textStyle={{fontSize: 14, marginLeft: 9}}
                  onPress={() => {
                    navigation.navigate('DetailJadwalSurvey');
                  }}>
                  Lihat Detail
                </Button>
              </View>
            </View>
          </CardList>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
