import axios from "axios";
import FormData from "form-data";
import qs from 'qs';
import { Platform } from "react-native";
import AppConfig from "../config/app";
import { store } from "../store";

axios.defaults.xsrfCookieName = 'OTHERCOOKIE';
axios.defaults.xsrfHeaderName = "X-OTHERNAME";
axios.defaults.withCredentials = false;

const request = () => {
    return axios.create({
        baseURL: AppConfig.BASE_URL,
        timeout: AppConfig.TIMEOUT,
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    });
}

const requestWithAuth = (useFormData = false) => {
    let user = store.getState().user;
    // let user = {
    //     token: "0bf1d1edc4d85c49944e5d3b6477b48e"
    // }
    // console.log("Token", user.token);

    return axios.create({
        baseURL: AppConfig.BASE_URL,
        timeout: AppConfig.TIMEOUT,
        headers: {
            "Accept": "application/json",
            "Content-Type": (useFormData ? "application/x-www-form-urlencoded" : "application/json"),
            "token": user.token,
        }
    });
}

export const HttpRequest = {
    login(data) {
        return request().post("/auth/login", data);
    },
    register(data) {
        return request().post("/auth/register", data);
    },
    resetPassword(data) {
        return request().post("/auth/reset-password", data)
    },
    resetPasswordVerify(data) {
        return request().post("/auth/reset-password-verify", data)
    },

    authProfile() {
        return requestWithAuth().get("/auth/profile")
    },

    //sales keranjang
    getSalesTipePembayaran() {
        return requestWithAuth().get("/sales/keranjang/list-tipe-pembayaran")
    },
    getSalesCaraPembayaran() {
        return requestWithAuth().get("/sales/keranjang/list-cara-pembayaran")
    },
    getSalesCaraPembayaranDp() {
        return requestWithAuth().get("/sales/keranjang/list-cara-pembayaran-dp")
    },
    getSalesKeranjangCustomer(keyword) {
        return requestWithAuth().get("/sales/keranjang/list-customer/" + "?keyword=" + keyword)
    },

    editSalesKeranjangCustomerAddres(customer_id, data) {
        return requestWithAuth().post("/sales/keranjang/add-alamat-customer/" + "?customer_id=" + customer_id, data)
    },

    postSelesKeranjangAddNewCustumer(data) {
        return requestWithAuth().post("/sales/keranjang/add-new-customer/", data)
    },
    postSelesKeranjangSave(data) {
        return requestWithAuth().post("/sales/keranjang/save/", data)
    },
    postSelesKeranjangSaveByID(id, data) {
        return requestWithAuth().post("/sales/keranjang/save/?id=" + id, data)
    },

    getSalesKeranjang(page, limit, keyword) {
        return requestWithAuth().get("/sales/keranjang/list/" + "?page=" + page + "&limit=" + limit + "&keyword=" + keyword)
    },
    getSalesKeranjangDetail(id) {
        return requestWithAuth().get("/sales/keranjang/detail/" + "?id=" + id)
    },

    postSalesKeranjangCheckOut(id, data) {
        return requestWithAuth().post("/sales/keranjang/checkout/" + "?keranjang_id=" + id, data)
    },

    postSalesKeranjangRincian(data) {
        return requestWithAuth().post("/sales/keranjang/hitung-rincian-potongan", data)
    },

    getDetailCustomerbyId(id) {
        return requestWithAuth().get("/sales/keranjang/detail-customer/" + "?id=" + id)
    },

    getProdukSatuan() {
        return request().get("/sales/keranjang/list-produk-satuan")
    },

    //sales produk
    getProdukKategoriSales() {
        return request().get("/sales/produk/list-produk-kategori")
    },
    getProdukSales(page, limit, kategori, keyword) {
        return request().get("/sales/produk/" + "?page=" + page + "&limit=" + limit + "&kategori_id=" + kategori + "&keyword=" + keyword)
    },
    getProdukDetailSales(id) {
        return request().get("/sales/produk/detail/" + "?id=" + id)
    },

    //sales tagihan terlambat 
    getTagihanTerlambat(page, limit, keyword) {
        return requestWithAuth().get("/sales/tagihan-terlambat/" + "?page=" + page + "&limit=" + limit + "&keyword=" + keyword)
    },
    getTagihanTerlambatById(id) {
        return requestWithAuth().get("/sales/tagihan-terlambat/detail-transaksi?id=" + id)
    },
    getTagihanTerlambatInvoiceById(id) {
        return requestWithAuth().get("/sales/tagihan-terlambat/detail?pelanggan_id=" + id)
    },

    //sales transaksi
    getSalesTransaksiList(page, limit, keyword, tab) {
        return requestWithAuth().get("/sales/transaksi/" + "?page=" + page + "&limit=" + limit + "&tab=" + tab + "&keyword=" + keyword)
    },
    getSalesTransaksiById(id) {
        return requestWithAuth().get("/sales/transaksi/detail?id=" + id)
    },


    //sales input bukti transfer 
    getSalesTotalPembayaran() {
        return requestWithAuth().get("/sales/pembayaran/total-semua-invoice")
    },
    getSalesTotalListBank() {
        return requestWithAuth().get("/sales/pembayaran/list-bank")
    },
    getSalesTotalListPelanggan() {
        return requestWithAuth().get("/sales/pembayaran/list-pelanggan")
    },
    postSalesTotalPembayaran(data) {
        return requestWithAuth().post("/sales/pembayaran/input-bukti", data)
    },

    //upload 
    postUpload(data) {
        return requestWithAuth().post("/image/upload", data)
    },
    postMutipleUpload(data) {
        return requestWithAuth().post("/image/upload-image", data)
    },

    //home 
    getHome(tgl) {
        return requestWithAuth().get("/sales/home/?tanggal=" + tgl)
    },

    //pelanggan
    getPelangganCustomer(page, limit, keyword) {
        return requestWithAuth().get("/sales/pelanggan/" + "?page=" + page + "&limit=" + limit + "&keyword=" + keyword)
    },
    getPelangganCustomerById(id) {
        return requestWithAuth().get("/sales/pelanggan/detail/?id=" + id)
    },
    postPelangganAdd(data) {
        return requestWithAuth().post("/sales/pelanggan/add/", data)
    },
    getPelangganLaporan(page, limit, customer_id, tab) {
        return requestWithAuth().get("/sales/pelanggan/laporan/" + "?page=" + page + "&limit=" + limit + "&customer_id=" + customer_id + "&tab=" + tab)
    },
    getPelangganLaporanById(id) {
        return requestWithAuth().get("/sales/pelanggan/laporan-detail-transaksi?penjualan_id=" + id)
    },

    getPelangganUpdate(id, data) {
        return requestWithAuth().post("/sales/pelanggan/update/?id=" + id, data)
    },
    getPelangganUpdateAlamat(customer_id, id, data) {
        return requestWithAuth().post("/sales/pelanggan/save-alamat/?customer_id=" + customer_id + "&id=" + id, data)
    },

    //cod
    getListCod(page, limit, keyword, tanggal) {
        return requestWithAuth().get("/sales/transaksi-cod/" + "?page=" + page + "&limit=" + limit + "&tanggal=" + tanggal + "&keyword=" + keyword)
    },

    //bulanan
    getListBulanan(page, limit, keyword, tanggal) {
        return requestWithAuth().get("/sales/transaksi-bulanan/" + "?page=" + page + "&limit=" + limit + "&tanggal=" + tanggal + "&keyword=" + keyword)
    },


    //customer
    homeCustomer(tanggal) {
        return requestWithAuth().get("/sales/home/?tanggal=" + tanggal)
    },
    getProdukCustomer(page, limit, kategori, keyword) {
        return requestWithAuth().get('/customer/produk/' + "?page=" + page + "&limit=" + limit + "&kategori_id=" + kategori + "&keyword=" + keyword)
    },
    getKategoriCustomer() {
        return requestWithAuth().get("/customer/produk/list-produk-kategori")
    },
    getProdukDetailCustomerById(id) {
        return requestWithAuth().get("/customer/produk/detail?id=" + id)
    },

    //keranjang
    getListTipePembayaranCustomer() {
        return requestWithAuth().get('/customer/keranjang/list-tipe-pembayaran')
    },
    getListCaraPembayaranCustomer() {
        return requestWithAuth().get('/customer/keranjang/list-cara-pembayaran')
    },
    getListCaraPembayaranDpCustomer() {
        return requestWithAuth().get("/customer/keranjang/list-cara-pembayaran-dp")
    },
    getListProdukProdukSatuanCustomer() {
        return requestWithAuth().get("/customer/keranjang/list-produk-satuan")
    },
    getListalamatCustomer() {
        return requestWithAuth().get("/customer/keranjang/list-alamat")
    },

    postAlamatCustomer(data) {
        return requestWithAuth().post("/customer/keranjang/add-alamat", data)
    },
    getListKeranjangCustomer(page, limit, keyword) {
        return requestWithAuth().get("/customer/keranjang/list/" + "?page=" + page + "&limit=" + limit + "&keyword=" + keyword)
    },
    getkeranjangDetailCustomerById(id) {
        return requestWithAuth().get("/customer/keranjang/detail/?id=" + id)
    },

    postKeranjangCustomerById(id, data) {
        return requestWithAuth().post("/customer/keranjang/save/?id=" + id, data)
    },
    postKeranjangCustomer(data) {
        return requestWithAuth().post("/customer/keranjang/save/", data)
    },
    postHitungRincianPotonganCustomer() {
        return requestWithAuth().post("/customer/keranjang/hitung-rincian-potongan")
    },
    postCustomerCheckoutKeranjang(keranjang_id, data) {
        return requestWithAuth().post("/customer/keranjang/checkout/?keranjang_id=" + keranjang_id, data)
    },


    CustomerPembayaranList() {
        return requestWithAuth().get("/customer/pembayaran/total-semua-invoice")
    },
    CustomerPembayaranListBank() {
        return requestWithAuth().get("/customer/pembayaran/list-bank")
    },
    CustomerPembayaranListInvoice() {
        return requestWithAuth().get("/customer/pembayaran/list-invoice")
    },
    CustomerPembayaranPost(data) {
        return requestWithAuth().post("/customer/pembayaran/input-bukti", data)
    },

    CustomerListTransaksi(page, limit, keyword, tab) {
        return requestWithAuth().get("/customer/transaksi/" + "?page=" + page + "&limit=" + limit + "&tab=" + tab + "&keyword=" + keyword)
    },
    CustomerListTransaksiById(id) {
        return requestWithAuth().get("/customer/transaksi/detail?id=" + id)
    },

    listPiutang(page, limit, keyword) {
        return requestWithAuth().get("/sales/piutang/" + "?page=" + page + "&limit=" + limit + "&keyword=" + keyword)
    },
    listPiutangById(id) {
        return requestWithAuth().get("/sales/piutang/detail/?id=" + id)
    },

    deletedKeranjang(id) {
        return requestWithAuth().get("/sales/keranjang/delete/?id=" + id)
    },


    resetByEmail(data) {
        return request().post("/auth/reset-password", data)
    },
    resetByEmailToken(data) {
        return request().post("/auth/reset-password-verify", data)
    }

};

export const FormDataConverter = {
    convert(data) {
        let form_data = new FormData();

        for (let key in data) {
            form_data.append(key, data[key]);
        }

        return form_data;
    }
};

export const HttpUtils = {
    normalizeUrl(url) {
        if (url != null) {
            return url.substr(0, url.indexOf("?"));
        }
        return null;
    }
};

export const HttpResponse = {
    processMessage(msg, alternateMessage = "Error processing data") {
        if (msg) {
            let data = msg.data;
            let messages = [];
            Object.keys(data).forEach((key) => {
                let arr = data[key];
                if (Array.isArray(arr)) {
                    messages.push(key + " - " + arr.join(" "));
                } else {
                    messages.push(key + " - " + arr);
                }
            });
            if (messages.length == 0) {
                return alternateMessage;
            }
            return messages.join(" ");
        }
        return alternateMessage;
    }
};