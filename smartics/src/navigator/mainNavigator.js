import React from 'react';
import {useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Login from '../screens/Login';
import Beranda from '../screens/Beranda';
import SplashScreen from '../screens/SplashScreen';
import Pelacakan from '../screens/Pelacakan';
import Perizinan from '../screens/Perizinan';
import Chat from '../screens/Chat';
import Profile from '../screens/Profile';
import Notifikasi from '../screens/Notifikasi';
import ListPemohonTerbaru from '../screens/ListPemohonTerbaru';
import AjukanPerizinan from '../screens/AjukanPerizinan';
import Panduan from '../screens/Panduan';
import Arsip from '../screens/Arsip';
import Ulasan from '../screens/Ulasan';
import Register from '../screens/Register';
import Intro from '../screens/Intro';
import UlasanDetail from '../screens/UlasanDetail';
import UlasanPelayanan from '../screens/UlasanPelayanan';
import DetailPerizinan from '../screens/DetailPerizinan';
import ChatDetail from '../screens/ChatDetail';
import LaporanSurvey from '../screens/LaporanSurvey';
import JadwalSurvey from '../screens/JadwalSurvey';
import Survey from '../screens/Survey';
import DetailLaporanSurvey from '../screens/DetailLaporanSurvey';
import DetailJadwalSurvey from '../screens/DetailJadwalSurvey';
import SemuaPerizinan from '../screens/SemuaPerizinan';
import PerizinanMasuk from '../screens/PerizinanMasuk';
import PerizinanTerlambat from '../screens/PerizinanTerlambat';
import ListPerizinanTerbaru from '../screens/ListPerizinanTerbaru';
import ListDaftarUlasan from '../screens/ListDaftarUlasan';
import LaporanAudit from '../screens/LaporanAudit';
import MenuPerizinan from '../screens/MenuPerizinan';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const AuthTabNavigator = () => {
  return (
    <Tab.Navigator initialRouteName="SplashScreen" tabBar={() => null}>
      <Tab.Screen
        name="Intro"
        component={Intro}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Login"
        component={Login}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Register"
        component={Register}
        options={() => ({headerShown: false})}
      />
    </Tab.Navigator>
  );
};

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="AuthTabNavigator"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="AuthTabNavigator" component={AuthTabNavigator} />
    </Stack.Navigator>
  );
};

const DashboardTabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Beranda"
      backBehavior={'none'}
      tabBar={() => null}>
      <Tab.Screen
        name="Beranda"
        component={Beranda}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Perizinan"
        component={Perizinan}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Pelacakan"
        component={Pelacakan}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={() => ({headerShown: false})}
      />
    </Tab.Navigator>
  );
};

const DashboardStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Dashboard"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Dashboard" component={DashboardTabNavigator} />
      <Stack.Screen name="Notifikasi" component={Notifikasi} />
      <Stack.Screen name="ListPemohonTerbaru" component={ListPemohonTerbaru} />
      {/* dashboard beranda utama */}
      <Stack.Screen name="AjukanPerizinan" component={AjukanPerizinan} />
      <Stack.Screen name="Panduan" component={Panduan} />
      <Stack.Screen name="Arsip" component={Arsip} />
      <Stack.Screen name="Ulasan" component={Ulasan} />
      <Stack.Screen name="UlasanDetail" component={UlasanDetail} />
      <Stack.Screen name="UlasanPelayanan" component={UlasanPelayanan} />
      <Stack.Screen name="DetailPerizinan" component={DetailPerizinan} />
      <Stack.Screen name="ChatDetail" component={ChatDetail} />

      {/* admin utama */}
      <Stack.Screen name="Survey" component={Survey} />
      <Stack.Screen name="LaporanSurvey" component={LaporanSurvey} />
      <Stack.Screen
        name="DetailLaporanSurvey"
        component={DetailLaporanSurvey}
      />
      <Stack.Screen name="JadwalSurvey" component={JadwalSurvey} />
      <Stack.Screen name="DetailJadwalSurvey" component={DetailJadwalSurvey} />

      <Stack.Screen
        name="ListPerizinanTerbaru"
        component={ListPerizinanTerbaru}
      />
      <Stack.Screen name="SemuaPerizinan" component={SemuaPerizinan} />
      <Stack.Screen name="PerizinanMasuk" component={PerizinanMasuk} />
      <Stack.Screen name="PerizinanTerlambat" component={PerizinanTerlambat} />
      <Stack.Screen name="ListDaftarUlasan" component={ListDaftarUlasan} />
      <Stack.Screen name="LaporanAudit" component={LaporanAudit} />

      <Stack.Screen name="MenuPerizinan" component={MenuPerizinan} />
    </Stack.Navigator>
  );
};

const AppContainer = () => {
  const user = useSelector(state => state.user);
  const splash = useSelector(state => state.splash);

  if (splash === true) {
    return <SplashScreen />;
  }

  if (user != null) {
    return (
      <>
        <NavigationContainer>
          <Tab.Navigator tabBar={() => null}>
            <Tab.Screen
              name="Dashboard"
              component={DashboardStack}
              options={() => ({headerShown: false})}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </>
    );
  } else {
    return (
      <NavigationContainer>
        <Tab.Navigator tabBar={() => null}>
          <Tab.Screen
            name="Auth"
            component={AuthNavigator}
            options={() => ({headerShown: false})}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
};

export default AppContainer;
